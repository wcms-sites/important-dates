core = 7.x
api = 2

; page_load_progress
projects[page_load_progress][type] = "module"
projects[page_load_progress][download][type] = "git"
projects[page_load_progress][download][url] = "https://git.uwaterloo.ca/drupal-org/page_load_progress.git"
projects[page_load_progress][download][tag] = "7.x-1.3+2-uw_wcms"
projects[page_load_progress][subdir] = ""

; views_datasource
projects[views_datasource][type] = "module"
projects[views_datasource][download][type] = "git"
projects[views_datasource][download][url] = "https://git.uwaterloo.ca/drupal-org/views_datasource.git"
projects[views_datasource][download][tag] = "7.x-1.0-alpha2"
projects[views_datasource][subdir] = ""

; uw_important_dates_cental_site
projects[uw_important_dates_central_site][type] = "module"
projects[uw_important_dates_central_site][download][type] = "git"
projects[uw_important_dates_central_site][download][url] = "https://git.uwaterloo.ca/wcms/uw_important_dates_central_site.git"
projects[uw_important_dates_central_site][download][tag] = "7.x-1.13"
projects[uw_important_dates_central_site][subdir] = ""
